//
//  main.swift
//  lw3
//
//  Created by Daniil Lushnikov on 26.09.2021.
//  Fibonacci numbers

// n - amount of returning values
let n = Int(readLine() ?? "") ?? 0
if 1 <= n && n <= 48 {
    if n == 1 {
        print(0)
    } else {
        let elements = fibonacci(n: n)
        for elem in elements {
            print(elem)
        }
    }
}

// returns fibonacci sequence
func fibonacci(n: Int) -> [Int] {
    var array = [0, 1]
    while array.count < n {
        array.append(array[array.count - 1] + array[array.count - 2])
    }
    return array
}
